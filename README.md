# Föli busstop emulator

## What is this about?
The idea of this project is to create a website/program, that emulates the dot-matrix screens that can be found on popular föli busstops.

## Why?
Why not? It would be fun to have a personal föli bus tracker, that may even remind you if you're going to be late for your bus. 
